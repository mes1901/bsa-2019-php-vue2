import Vue from "vue";
import router from "../../../router";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);

const state = {
  photos: []
};

const getters = {
  getPhotosByAlbumId: state => id => {
    return state.photos.filter(photo => photo.albumId === id);
  },
  getPhotoById: state => id => {
    return state.photos.filter(photo => photo.id === id)[0];
  }
};

const actions = {
  loadPhotos({ commit }) {
    axios.get("https://jsonplaceholder.typicode.com/photos").then(response => {
      commit("SET_PHOTOS", response.data);
    });
  },
  deletePhoto({ commit }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit("DELETE_PHOTO", data);
        resolve();
      }, 250);
    });
  }
};

const mutations = {
  SET_PHOTOS(state, photos) {
    state.photos = photos;
  },
  DELETE_PHOTO(state, data) {
    const ind = state.photos.findIndex(photo => photo.id === data.photoId);
    if (ind !== -1) {
      state.photos.splice(ind, 1);
      router.push({
        name: "showAlbum",
        params: { id: data.albumId }
      });
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
