import axios from "axios";

const upload = (url, data) => {
  const fd = new FormData();
  fd.append("image", data);
  return axios.post(url, fd);
};

const uploadService = {
  upload
};

export default uploadService;
