import Vue from "vue";
import router from "../../../router";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);

let lastAlbumId = function(state) {
  let id = 0;
  state.albums.map(album => {
    if (album.id > id) {
      id = album.id;
    }
  });
  return id;
};

const state = {
  albums: []
};

const getters = {
  getAlbumById: state => id => {
    return state.albums.find(album => album.id === id);
  }
};

const actions = {
  loadAlbums({ commit }) {
    axios.get("https://jsonplaceholder.typicode.com/albums").then(response => {
      commit("SET_ALBUMS", response.data);
    });
  },
  editAlbum({ commit }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit("EDIT_ALBUM", data);
        resolve();
      }, 250);
    });
  },
  addAlbum({ commit }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit("ADD_ALBUM", data);
        resolve();
      }, 250);
    });
  },
  deleteAlbum({ commit }, albumId) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit("DELETE_ALBUM", albumId);
        resolve();
      }, 250);
    });
  }
};

const mutations = {
  SET_ALBUMS(state, albums) {
    state.albums = albums;
  },
  EDIT_ALBUM(state, { albumId, data }) {
    const ind = state.albums.findIndex(album => album.id === albumId);
    if (ind !== -1) {
      const updatedAlbum = {
        id: albumId,
        title: data.title,
        userId: data.userId
      };
      Vue.set(state.albums, ind, updatedAlbum);
    }
  },
  ADD_ALBUM(state, album) {
    state.albums.push({
      id: lastAlbumId(state) + 1,
      title: album.title,
      userId: 1
    });
  },
  DELETE_ALBUM(state, albumId) {
    const ind = state.albums.findIndex(album => album.id === albumId);
    if (ind !== -1) {
      state.albums.splice(ind, 1);
      router.replace("/albums");
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
