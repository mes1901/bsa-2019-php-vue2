import Vue from "vue";
import Router from "vue-router";
import Users from "./views/UsersList.vue";
import Albums from "./views/AlbumsList.vue";
import ShowUser from "./components/ShowUser.vue";
import ShowAlbum from "./components/ShowAlbum.vue";
import EditUser from "./components/UserEdit.vue";
import EditAlbum from "./components/AlbumEdit.vue";
import CreateUser from "./components/CreateUser.vue";
import CreateAlbum from "./components/CreateAlbum.vue";
import ShowPhoto from "./components/ShowPhoto.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: "/",
  routes: [
    { path: "/", name: "users", component: Users },
    { path: "/albums", name: "albums", component: Albums },
    { path: "/users/:id", name: "showUser", component: ShowUser },
    { path: "/albums/:id", name: "showAlbum", component: ShowAlbum },
    { path: "/users/:id/edit", name: "editUser", component: EditUser },
    { path: "/albums/:id/edit", name: "editAlbum", component: EditAlbum },
    { path: "/users/add", name: "newUser", component: CreateUser },
    { path: "/albums/add", name: "newAlbum", component: CreateAlbum },
    { path: "/albums/:albumId/photo/:photoId", name: "showPhoto", component: ShowPhoto },
  ]
});
