import Vue from "vue";
import router from "../../../router";
import axios from "axios";
import VueAxios from "vue-axios";

Vue.use(VueAxios, axios);

let lastUserId = function(state) {
  let id = 0;
  state.users.map(user => {
    if (user.id > id) {
      id = user.id;
    }
  });
  return id;
};

const state = {
  users: []
};

const getters = {
  getUserById: state => id => {
    return state.users.find(user => user.id === id);
  },
  getLastId: state => {
    let id = 0;
    state.users.map(user => {
      if (user.id > id) {
        id = user.id;
      }
    });
    return id;
  }
};

const actions = {
  loadUsers({ commit }) {
    axios.get("https://jsonplaceholder.typicode.com/users").then(response => {
      commit("SET_USERS", response.data);
    });
  },
  editUser({ commit }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit("EDIT_USER", data);
        resolve();
      }, 250);
    });
  },
  addUser({ commit }, data) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit("ADD_USER", data);
        resolve();
      }, 250);
    });
  },
  deleteUser({ commit }, userId) {
    return new Promise(resolve => {
      setTimeout(() => {
        commit("DELETE_USER", userId);
        resolve();
      }, 250);
    });
  }
};

const mutations = {
  SET_USERS(state, users) {
    state.users = users;
  },
  EDIT_USER(state, { userId, data }) {
    const ind = state.users.findIndex(user => user.id === userId);
    if (ind !== -1) {
      const updatedUser = {
        id: userId,
        name: data.name,
        email: data.email,
        address: data.address
      };
      Vue.set(state.users, ind, updatedUser);
    }
  },
  ADD_USER(state, user) {
    state.users.push({
      id: lastUserId(state) + 1,
      name: user.name,
      email: user.email,
      address: user.address
    });
  },
  DELETE_USER(state, userId) {
    const ind = state.users.findIndex(user => user.id === userId);
    if (ind !== -1) {
      state.users.splice(ind, 1);
      router.replace("/");
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
